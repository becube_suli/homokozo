# Homokozó

Ez a projekt a BeCube programozóiskola haladó tanfolyamának 8., és 9. alkalmához készült. Célja, hogy gyakorolhasd a létezó repó klónozását, illetve legyen egy repó, amit lelkiismeretfurdalás nélkül módosíthatsz.

A tertalom nem számít, bárki bármit beleírhat, módosíthat, törölhet.

Miután letöltötted és telepítetted a Gitet, így klónozd le ezt a repót:

```
git clone https://gitlab.com/becube_suli/homokozo.git
```

